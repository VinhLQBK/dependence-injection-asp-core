﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository.Interfaces;

namespace WebApp.Repository.Implements
{
    public class HomeRepositoryIm: HomeRepository
    {
        public string GetMessage()
        {
            return "Home";
        }
    }
}
